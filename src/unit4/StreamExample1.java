package unit4;

import unit1.Person;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamExample1 {
    public static void main(String args[]) {
        List<Person> people = Arrays.asList(
            new Person("mayuri","waghmode",29),
            new Person("ishan","bhale",29),
            new Person("jairam","dixit",29),
            new Person("empw","bed",29));
        people.stream()
                .filter((p)->p.getLastname().startsWith("w"))
                .forEach(p->System.out.println(p.getFirstname()));
        Stream<Person> s = people.stream();
        System.out.println(s);
        long cnt = people.parallelStream()
                .filter((p)->p.getLastname().startsWith("w"))
                .count();
        System.out.println(cnt);

    }
}
