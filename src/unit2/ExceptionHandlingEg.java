package unit2;

import java.util.function.BiConsumer;

public class ExceptionHandlingEg {
    public static void main(String args[]) {
        int[] arr = {1,2,3,4,5};
        int key = 0;
        process(arr, key ,wrapperLambda( (a,b)-> System.out.println(a/b)));
    }
    public static void process(int[] arr, int key, BiConsumer<Integer,Integer> biConsumer){
        for(int i : arr) {
            //try {
                biConsumer.accept(i, key);
            //}catch (ArithmeticException a){}
        }
    }
    private static BiConsumer<Integer,Integer> wrapperLambda(BiConsumer<Integer,Integer> biConsumer){
        return (a,b)-> {
            try {
                System.out.println("from wrapper");
                biConsumer.accept(a, b);
            }catch (ArithmeticException e){System.out.println("Exception catch");}
        };
    }
}
