package unit3;

public class MethodReferenceExample {
    public static void main(String args[]) {
        Thread t = new Thread(()->message());//if lambda expression is pass through i.e just method execution or lambda without input arguments
        t.start();
        //in this case use use method reference  ---> unit3.MethodReferenceExample::message === ()-> message();
        Thread t1 = new Thread(MethodReferenceExample::message);
        t1.start();
    }
    public static  void message(){
        System.out.println("Hwloo");
    }
    }
