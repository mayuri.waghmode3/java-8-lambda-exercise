package unit3;

public class ThisReferenceExample {
    public void doProcess(int i, Process p){
        p.process(i);
    }

    public static void main(String args[]){
        ThisReferenceExample thisReferenceExample = new ThisReferenceExample();
        //System.out.println(this);
//        thisReferenceExample.doProcess(10, new unit2.ClosureExample.Process() {
//            @Override
//            public void process(int i) {
//                System.out.println("value of i : "+i);
//                System.out.println(this);
//            }
//            @Override
//            public String toString(){
//                return "this is anonymous Class instance";
//            }
//        });
        //System.out.println(this);
        //lambda treats this as outside which is static context as above
        thisReferenceExample.doProcess(10, (i)->{
            System.out.println("value of i : "+i);
            //System.out.println(this);
        });
        thisReferenceExample.exectute();
    }
    public void exectute(){
        doProcess(12,(i)->{
            System.out.println("value of i : "+i);
            System.out.println(this);
        });
    }
    @Override
    public String toString(){
        return "this is Current Class instance";
    }

    interface Process{
        void process(int i);
    }
}
