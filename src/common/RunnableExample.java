package common;

public class RunnableExample {
    public static void main(String args[]) {
        Thread myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Printed inside runnable");
            }
        });
        Thread myThreadLambda = new Thread(()-> System.out.println("Printed inside runnable by lambda"));
        myThread.run();
        myThreadLambda.start();
    }
}
