package common;

@FunctionalInterface
public interface Greeting {

    //functional interface has 1 abstract method
    void perform();
    //void oppps(int opss); // not allowed

}
