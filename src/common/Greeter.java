package common;

public class Greeter {
    public void greet(Greeting greeting){
        greeting.perform();
    }
    public static void main(String args[]){
        Greeter greeter = new Greeter();
        HelloWorldGreeting helloWorldGreeting = new HelloWorldGreeting();
        greeter.greet(helloWorldGreeting);

        MyGreeting myGreeting = ()->{System.out.println("Hello World!!  --> Lambda function1");};
        myGreeting.perform();

        //lambda function is almost like behaving like implementation of interface
        Greeting myGreetingLambda = ()->{System.out.println("Hello World!!  --> Lambda function2");};
        greeter.greet(myGreetingLambda);

        //binding  lambda expression to greeting interface --> type inference
        greeter.greet(()->System.out.println("Hello World!!  --> Lambda function3"));

        //anonymous inner class
        Greeting myAnonymousGreetinClass = new Greeting(){
            public void perform(){
                System.out.println("Hello World!!  --> Anonymous inner class  ");
            }
        };
        myAnonymousGreetinClass.perform();

        //   addFunction = (int a, int b) -> a+b;
        //   safeDivide = (int a , int b) ->{
        //       if(b==0) return 0;
        //       else return a/b;
        //   };
        //   lengthString = (int s)-> s.length();

    }
    //Functional interface
    interface MyGreeting {
        void perform ();
        //void oopss(int a);
    }
}
