package common;

public class TypeInferenceExample {
    public static void main(String args[]) {
        StringLengthLambda obj =  s-> s.length();

        System.out.println(obj.calLength("Hello Lambda"));

        TypeInferenceExample.printLambda(s-> s.length());
    }
    public static void printLambda(StringLengthLambda l){
        System.out.println(l.calLength("HEllo Lambdaa"));
    }
    interface StringLengthLambda{
        int calLength(String s);
    }
}
