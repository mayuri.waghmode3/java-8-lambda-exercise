package unit1;

import java.util.*;

public class Unit1ExerciseJava8 {
    public static void main(String args[]) {
        List<Person> persons = Arrays.asList(
                new Person("mayuri","waghmode",29),
                new Person("ishan","bhale",29),
                new Person("jairam","dixit",29),
                new Person("empw","bed",29)
        );

        //Sort list by last name --> java 8
        Collections.sort(persons,(o1,o2)->o1.getLastname().compareTo(o2.getLastname()));

        //create a method that prints all elements in list ---> java 8
        printConditionally(persons,p -> true);
        //persons.forEach(person -> System.out.println(person));

        // create a method that prints all people with lastname starts with b --> java 8
        printConditionally(persons, p -> p.getLastname().startsWith("b"));
        printConditionally(persons, p -> p.getFirstname().startsWith("m"));
    }
    interface Condition{
        boolean byCondition(Person person);
    }
    private static void printConditionally(List<Person> personList, Condition condition){
        for(Person person : personList)
            if(condition.byCondition(person))
                System.out.println(person);
    }
}
