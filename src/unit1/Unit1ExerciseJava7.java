package unit1;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Unit1ExerciseJava7 {
    public static void main(String args[]) {
        List<Person> persons = Arrays.asList(
                new Person("mayuri","waghmode",29),
                new Person("ishan","bhale",29),
                new Person("jairam","dixit",29),
                new Person("empw","bed",29)
        );
        //Sort list by last name --> java 7
        Collections.sort(persons, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getLastname().compareTo(o2.getLastname());
            }
        });

        //create a method that prints all elements in list ---> java 7
        Unit1ExerciseJava7.printAll(persons);

        // create a method that prints all people with lastname starts with b --> java 7
        printConditionally(persons,new Condition(){
            @Override
            public boolean byCondition(Person person) {
                return  (person.getLastname().startsWith("b"));
            }
        });
        // create a method that prints all people with firstname starts with m --> java 7
        printConditionally(persons,new Condition(){
            @Override
            public boolean byCondition(Person person) {
                return person.getFirstname().startsWith("m");
            }
        });

    }
    interface Condition{
        boolean byCondition(Person person);
    }
    private static void printConditionally(List<Person> personList, Condition condition){
        for(Person person : personList)
            if(condition.byCondition(person))
                System.out.println(person);

    }
    private static void printAll(List<Person> personList){
        for(Person person : personList)
            System.out.println(person);
    }

}
