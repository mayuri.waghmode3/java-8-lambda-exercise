package unit1;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class StandardFunctionInterfaceExample {
    public static void main(String args[]) {
        List<Person> persons = Arrays.asList(
                new Person("mayuri","waghmode",29),
                new Person("ishan","bhale",29),
                new Person("jairam","dixit",29),
                new Person("empw","bed",29)
        );
        //Sort list by last name --> java 8
        Collections.sort(persons,(o1,o2)->o1.getLastname().compareTo(o2.getLastname()));

        //create a method that prints all elements in list ---> java 8
        printConditionally(persons,p -> true,p ->System.out.println(p));
        //persons.forEach(person -> System.out.println(person));

        // create a method that prints all people with lastname starts with b --> java 8
        printConditionally(persons, p -> p.getLastname().startsWith("b"), p ->System.out.println(p.getLastname()));
        printConditionally(persons, p -> p.getFirstname().startsWith("m"), p ->System.out.println(p.getFirstname()));
    }
    private static void printConditionally(List<Person> personList, Predicate<Person> predicate, Consumer<Person> consumer) {
        for(Person person : personList)
            if(predicate.test(person))
                consumer.accept(person);
    }
}
